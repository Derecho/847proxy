# 847proxy
Workaround for missing bidirectional CAT control on early Yaesu FT-847 models,
for use with hamlib or other application's existing FT-847 support.

## Description
The early models of the FT-847 radio made by Yaesu have a bug in their firmware
causing them not to reply to any commands. This is especially troublesome for
the commands used to read the frequency and mode of the radio. Most applications
available on the internet with FT-847 support assume this functionality works
properly, which results in unexpected behaviour and crashes when used with an
early model.

This project aims to work around that issue by offering the user a proxy that
intercepts the problematic commands and fakes an appropriate reply. On starting
847proxy the radio is set to initial values and the proxy will keep track of
any changes as long as the radio is only controlled via CAT commands.

## Usage
847proxy has been written for use on a Linux computer but should be mostly
POSIX compliant (though it may depend on glibc). Windows is not supported.

The project can be compiled by simply issuing a `make` command, after which the
proxy can be started with:

    ./847proxy /dev/ttyUSB0

The above example assumes your radio's CAT interface is available at
`/dev/ttyUSB0`, change accordingly if this is not the case. After running this
command, a virtual port will be created, for example at `/dev/pts/5`.

Use the virtual port for any applications that assume a late model FT-847.
At the moment RTS/DTR is not passed through, so if you use that for PTT make
sure your PTT settings are still set to use the real CAT interface path.

A baudrate setting of 57600 is assumed.

## Status
847proxy has some room for improvement regarding configurability, but at the
moment solves the main issue and can be used for its intended purpose.

Known working applications:

   * wsjtx
   * rigctld

Fldigi may be reluctant to send commands to the virtual port for some as of yet
unknown reason. However, fldigi can be set up to use flrig for controlling your
radio, and flrig seems to work fine with the direct radio port.
