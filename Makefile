.PHONY: all clean

all: 847proxy

847proxy: 847proxy.c
	$(CC) -o $@ $<

clean:
	rm 847proxy
