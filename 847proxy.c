#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

static const int PACKETSIZE = 5;
static const int DELAY = 200000;
static const unsigned char RX_STATUS_REPLY = 0b00000000;  // CTCSS matched, Squelch off
static const unsigned char TX_STATUS_REPLY = 0b10000000;  // PTT Off

void set_rts(int fd, char on)
{
    int status;
    ioctl(fd, TIOCMGET, &status);
    if(on)
        status |= TIOCM_RTS;
    else
        status &= ~TIOCM_RTS;
    ioctl(fd, TIOCMSET, &status);
}

void set_dtr(int fd, char on)
{
    int status;
    ioctl(fd, TIOCMGET, &status);
    if(on)
        status |= TIOCM_DTR;
    else
        status &= ~TIOCM_DTR;
    ioctl(fd, TIOCMSET, &status);
}

int open_radio_port(char *path)
{
    int fd = open(path, O_RDWR | O_NOCTTY);
    if(fd == -1) {
        printf("Failed to open the radio port\n");
        exit(1);
    }

    // Set RTS and DTR low (PTT off)
    // Make user selectable option?
    set_rts(fd, 0);
    set_dtr(fd, 0);

    // Set an explicit baudrate
    struct termios opts;
    tcgetattr(fd, &opts);
    // TODO Let the user choose a baudrate
    cfsetispeed(&opts, B57600);
    cfsetospeed(&opts, B57600);
    tcsetattr(fd, TCSANOW, &opts);

    return fd;
}

void init_radio(int fd, unsigned char *main_vfo_mode, unsigned char *satrx_vfo_mode, unsigned char *sattx_vfo_mode)
{
    // Throughout this function, PACKETSIZE is blatantly ignored, as all
    // commands are hardcoded anyway and changing the size wouldn't make sense.

    unsigned char *cmd_queue[8];  // See cmd_queue population below for the required size

    unsigned char cat_on[] = {0x00, 0x00, 0x00, 0x00, 0x00};
    unsigned char cat_off[] = {0x00, 0x00, 0x00, 0x00, 0x80};

    // Prepare commands with opcodes
    unsigned char main_mode[] = {0x00, 0x00, 0x00, 0x00, 0x07};
    unsigned char satrx_mode[] = {0x00, 0x00, 0x00, 0x00, 0x17};
    unsigned char sattx_mode[] = {0x00, 0x00, 0x00, 0x00, 0x27};
    // Set modes
    main_mode[0] = main_vfo_mode[4];
    satrx_mode[0] = satrx_vfo_mode[4];
    sattx_mode[0] = sattx_vfo_mode[4];

    unsigned char main_freq[5], satrx_freq[5], sattx_freq[5];
    // Copy over frequency data (same positions)
    memcpy(main_freq, main_vfo_mode, 4);
    memcpy(satrx_freq, satrx_vfo_mode, 4);
    memcpy(sattx_freq, sattx_vfo_mode, 4);
    // Set opcodes
    main_freq[4] = 0x01;
    satrx_freq[4] = 0x11;
    sattx_freq[4] = 0x21;

    // Add all commands to the queue
    cmd_queue[0] = cat_on;
    cmd_queue[1] = main_mode;
    cmd_queue[2] = satrx_mode;
    cmd_queue[3] = sattx_mode;
    cmd_queue[4] = main_freq;
    cmd_queue[5] = satrx_freq;
    cmd_queue[6] = sattx_freq;
    cmd_queue[7] = cat_off;

    // Send off all commands
    printf("Initialising radio\n");
    int i;
    for(i = 0; i < sizeof(cmd_queue)/sizeof(unsigned char *); i++) {
        int written = write(fd, cmd_queue[i], 5);
        // TODO Verbosity flag
        //printf("Bytes written: %d\n", written);
        if(written != 5) {
            printf("Writing to radio failed (init)\n");
            exit(1);
        }
        usleep(DELAY);
    }
}

// Make sure slave_path is large enough!
int create_pseudoterminal(char *slave_path)
{
    // Or use getpt(3) or posix_openpt(3)?
    int fd = open("/dev/ptmx", O_RDWR | O_NOCTTY);
    if(fd == -1) {
        printf("Failed to open ptmx\n");
        exit(1);
    }

    if(grantpt(fd) != 0) {
        printf("Failed to set permissions of slave pseudoterminal\n");
        exit(1);
    }

    // Set an explicit baudrate
    struct termios opts;
    tcgetattr(fd, &opts);
    // TODO Let the user choose a baudrate
    cfsetispeed(&opts, B57600);
    cfsetospeed(&opts, B57600);
    tcsetattr(fd, TCSANOW, &opts);

    if(unlockpt(fd) != 0) {
        printf("Failed to unlock slave pseudoterminal\n");
    }

    if(slave_path != NULL)
        strcpy(slave_path, ptsname(fd));
    return fd;
}

int main(int argc, char *argv[])
{
    if(argc < 2) {
        printf("Usage: %s <path_to_radio_port>\n", argv[0]);
        exit(0);
    }

    // Disable stdout buffering
    setbuf(stdout, NULL);

    // TODO Take baudrate from argument

    // Open serial port to radio
    int radio_port = open_radio_port(argv[argc-1]);  // Last cli argument

    // Make up an initial state for the VFOs and demodulator
    // TODO Make user configurable
    // 100MHz+10MHz, 1MHz+100KHz, 10+1kHz, 100+10Hz, Mode
    unsigned char main_vfo_mode[] = {0x01, 0x40, 0x70, 0x00, 0x01};  // 14.070 MHz USB
    unsigned char satrx_vfo_mode[] = {0x14, 0x50, 0x00, 0x00, 0x01};  // 145.000 MHz USB
    unsigned char sattx_vfo_mode[] = {0x43, 0x50, 0x00, 0x00, 0x00};  // 435.000 MHz LSB

    // Send initial state to radio
    init_radio(radio_port, main_vfo_mode, satrx_vfo_mode, sattx_vfo_mode);

    // Recreate the port when it gets closed by the other process
    while(1) {
        // Set up virtual serial port
        char virtual_port_path[32];  // "/dev/pts/X" is 11 bytes, plus some spare
        int virtual_port = create_pseudoterminal((char *)&virtual_port_path);

        printf("Virtual serial port created at: %s\n", virtual_port_path);

        // Pass all packets except the "read frequency & mode status" ones
        unsigned char buffer[PACKETSIZE];
        int offset = 0;
        int bytes_read;
        while((bytes_read = read(virtual_port, buffer + offset, PACKETSIZE - offset)) > 0) {
            // TODO Verbosity flag
            //printf("Bytes read: %d\n", bytes_read);

            if(bytes_read + offset != PACKETSIZE) {
                // Either we are appending to an earlier packet, or we received a
                // new incomplete one.
                offset += bytes_read;
                continue;
            }

            // If we're here, the packet must be complete.

            offset = 0;  // Clear any possible offset

            printf("Received packet on virtual port: ");
            int i;
            for(i = 0; i < PACKETSIZE; i++)
                printf("%hhX ", buffer[i]);
            printf("\n");

            // Is it a command we need to fake a response for?
            unsigned char opcode_nibble = buffer[4] & 0x0F;
            if(opcode_nibble == 0x03 || opcode_nibble == 0x07) {  // Opcodes 03, 13, 23, e7, f7
                printf("Intercepting packet\n");
                const unsigned char *reply;
                int size;

                switch(buffer[4]) {
                    case 0x03:
                        reply = main_vfo_mode;
                        size = PACKETSIZE;
                        break;
                    case 0x13:
                        reply = satrx_vfo_mode;
                        size = PACKETSIZE;
                        break;
                    case 0x23:
                        reply = sattx_vfo_mode;
                        size = PACKETSIZE;
                        break;
                    case 0xE7:
                        reply = &RX_STATUS_REPLY;
                        size = 1;
                        break;
                    case 0xF7:
                        reply = &TX_STATUS_REPLY;
                        size = 1;
                        break;
                    default:
                        printf("Invalid command received on virtual port\n");
                        exit(1);
                }

                if(write(virtual_port, reply, size) != size) {
                    printf("Writing patched reply to virtual port failed\n");
                    exit(1);
                }
                continue;
            }
            // Is a mode or frequency being updated?
            else if(opcode_nibble == 0x01 || opcode_nibble == 0x07) {  // Opcodes 01, 11, 21, 07, 17, 27
                printf("Updating internal info\n");
                unsigned char *mode;
                unsigned char freq = 0;
                
                switch(buffer[4]) {
                    case 0x01:
                        freq = 1;
                    case 0x07:
                        mode = main_vfo_mode;
                        break;
                    case 0x11:
                        freq = 1;
                    case 0x17:
                        mode = satrx_vfo_mode;
                        break;
                    case 0x21:
                        freq = 1;
                    case 0x27:
                        mode = sattx_vfo_mode;
                        break;
                    default:
                        printf("Invalid command received on virtual port\n");
                        exit(1);
                }

                if(freq)
                    memcpy(mode, buffer, 4);
                else
                    mode[4] = buffer[0];
            }

            printf("Passing packet on\n");
            if(write(radio_port, buffer, PACKETSIZE) != PACKETSIZE) {
                printf("Writing to radio failed\n");
                exit(1);
            }
        }

        close(virtual_port);
    }
    return 0;
}
